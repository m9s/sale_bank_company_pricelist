#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Sale Bank Company Pricelist',
    'name_de_DE': 'Verkauf Bankverbindung Unternehmen mit Preislisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Sale Bank Company with Pricelists
''',
    'description_de_DE': '''Verkauf Bankverbindung Unternehmen in
    Verbindung mit Preislisten
    - Führt notwendige Funktionen bei Verwendung der Module
      sale_bank_company und sale_pricelist zusammen.
''',
    'depends': [
        'account_invoice_bank_company',
        'party_bank',
        'sale',
        'sale_bank_company',
        'sale_pricelist',
    ],
    'xml': [
    ],
}
